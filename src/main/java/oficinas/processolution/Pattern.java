/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution;

/**
 *
 * @author geova
 */
public class Pattern {
    private String cor;
    private Integer size;
    private Integer qntd;

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getQntd() {
        return qntd;
    }

    public void setQntd(Integer qntd) {
        this.qntd = qntd;
    }

    public Pattern(String cor, Integer size, Integer qntd) {
        this.cor = cor;
        this.size = size;
        this.qntd = qntd;
    }    
    
    public Pattern(){
        
    }
}
