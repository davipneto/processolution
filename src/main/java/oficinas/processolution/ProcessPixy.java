/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author davi
 */
public class ProcessPixy {
    public static void main(String[] argv) throws IOException{
        int menoresX[] = new int[2];
        menoresX[0] = 300;
        menoresX[1] = 300;
        Timer timer = new Timer();
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                ProcessThread t;
                ArrayList<Frame> frames = new ArrayList();
                String sig;
                int x, y, width, height, day, month, year, hour, min, sec, frameID;
                int start, end, i, j, menorPosicaoFrame=1000, menorPosicaoAtual=1000;
                try {
                    BufferedReader br = new BufferedReader(new FileReader("C:/Users/geova/Desktop/teste_pixy/output.txt"));
                    String text = br.readLine();
                    while(!text.contains("frame")){
                        text = br.readLine();
                    }
                    //System.out.println("ACHO O FRAME");
                    //System.out.println(text);
                    //text = br.readLine();
                    //System.out.println("Primeiro negocio: " + text);
                    while (text != null){
                        //System.out.println(text);
                        if(text.contains("frame")){
                            ArrayList<Product> products = new ArrayList();
                            start = text.indexOf("frame") + 6;
                            end = text.indexOf(":", start);
                            frameID = Integer.parseInt(text.substring(start, end));
                            text = br.readLine();
                            while(text!= null && text.contains("sig")){
                                //get the informacions for each object on the running machine at that frame
                                start = text.indexOf("sig") + 5;
                                end = text.indexOf(" ", start);
                                sig = text.substring(start, end);
                                //System.out.println("sig: " + sig);
                                start = end + 4;
                                end = text.indexOf(" ", start);
                                x = Integer.parseInt(text.substring(start, end));
                                //System.out.println("x: " + x);
                                start = end + 4;
                                end = text.indexOf(" ", start);
                                y = Integer.parseInt(text.substring(start, end));
                                //System.out.println("y: " + y);
                                start = end + 8;
                                end = text.indexOf(" ", start);
                                width = Integer.parseInt(text.substring(start, end));
                                //System.out.println("width: " + width);
                                start = end + 9;
                                end = text.indexOf(" ", start);
                                height = Integer.parseInt(text.substring(start, end));
                                //System.out.println("height: " + height);
                                start = end + 7;
                                day = Integer.parseInt(text.substring(start, start + 2));
                                //System.out.println("Day: " + day);
                                start += 3;
                                month = Integer.parseInt(text.substring(start, start + 2));
                                //System.out.println("Month: " + month);
                                start += 3;
                                year = Integer.parseInt(text.substring(start, start + 4));
                                //System.out.println("Year: " + year);
                                start += 5;
                                hour = Integer.parseInt(text.substring(start, start + 2));
                                //System.out.println("Hour: " + hour);
                                start += 3;
                                min = Integer.parseInt(text.substring(start, start + 2));
                                //System.out.println("Min: " + min);
                                start += 3;
                                sec = Integer.parseInt(text.substring(start, start + 2));
                                //System.out.println("Sec: " + sec);
                                products.add(new Product(sig, x, y, width, height, new DateProcess(year, month, day, hour, min, sec)));
                                /*i = 12;
                                do{
                                    posicaoX = posicaoX.concat(String.valueOf(text.charAt(i)));
                                    i++;
                                }while(text.charAt(i) != ' ');
                                System.out.println(Integer.parseInt(posicaoX));
                                if(Integer.parseInt(posicaoX) < menorPosicaoFrame){
                                    menorPosicaoFrame = Integer.parseInt(posicaoX);
                                }*/
                                //posicaoX = "";
                                text = br.readLine();
                            }
                            Collections.sort(products);
                            frames.add(new Frame(frameID, products));
                            //products.clear();
                            /*System.out.println("Menor Posicao do Frame: " + menorPosicaoFrame);
                            if(menorPosicaoFrame < menorPosicaoAtual){
                                System.out.println("Entrou Algo Novo");
                            }
                            menorPosicaoAtual = menorPosicaoFrame;
                            menorPosicaoFrame = 1000;*/
                        }
                        //text = br.readLine();
                    }
                    br.close();
                    Frame[] framesArray = new Frame[frames.size()];
                    t = new ProcessThread(frames.toArray(framesArray), menoresX);
                    t.start();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ProcessPixy.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(ProcessPixy.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        TimerTask task_cancel = new TimerTask(){
            @Override
            public void run() {
                System.out.println("Terminou!");
                timer.cancel();
            }
        
        };
        Calendar calendar = Calendar.getInstance();
//        calendar.set(Calendar.HOUR_OF_DAY, 18);
//        calendar.set(Calendar.MINUTE, 34);
//        calendar.set(Calendar.SECOND, 0);
        Date time = calendar.getTime();
        System.out.println("Começou!");
        timer.scheduleAtFixedRate(task, 0, 10000);
        //calendar.set(Calendar.HOUR_OF_DAY, 23);
        //calendar.set(Calendar.MINUTE, 59);
        //calendar.set(Calendar.SECOND, 59);
        //time = calendar.getTime();
        //timer.schedule(task_cancel, time);
        
    }
}
