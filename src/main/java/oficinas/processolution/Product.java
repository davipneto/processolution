/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution;

/**
 *
 * @author davi
 * @author Geovana
 */
public class Product implements Comparable<Product>{
    
    private String sig;
    private int x;
    private int y;
    private int width;
    private int height;
    private DateProcess date;
    private Integer ID;
    
    public Product(String sig, int x, int y, int width, int height, DateProcess date) {
        this.sig = sig;
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.date = date;
    }
    
    public String getSig() {
        return sig;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public DateProcess getDate() {
        return date;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    @Override
    public String toString() {
        return "Product{" + "sig=" + sig + ", x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + ", date=" + date + '}';
    }

    @Override
    public int compareTo(Product o) {
        if (this.x > o.x)
            return 1;
        if (this.x < o.x)
            return -1;
        return 0;
    }
    
    
}
