/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution;

import java.util.List;

/**
 *
 * @author davi
 */
public class Frame {
    
    private int frameID;
    private List<Product> products;

    public Frame(int frameID, List<Product> products) {
        this.frameID = frameID;
        this.products = products;
    }

    public int getFrameID() {
        return frameID;
    }
    
    public List<Product> getProducts() {
        return products;
    }

    @Override
    public String toString() {
        String print = "Frame " + frameID + ": " + "\n";
        for (int i=0; i<products.size(); i++){
            print = print.concat(products.get(i).toString() + "\n");
        }
        return print;
    }
    
    
}
