/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution.dao;

import conection.Conection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import oficinas.processolution.Alert;
import oficinas.processolution.DateProcess;

/**
 *
 * @author geova
 */
public class AlertDAO{
    private Connection conexao;

    public AlertDAO() throws ClassNotFoundException, SQLException{
        this.conexao = Conection.getConection();
    }
    
    public void incluir (Alert alert) throws SQLException{
            String sqlincluir = "insert into alert (Texto, Data, Horario, Importancia) values(?,?,?,?)";
        try (PreparedStatement smt = conexao.prepareStatement(sqlincluir)) {
            smt.setString (1,alert.getText());
            smt.setString (2,alert.getDate().toDateDataBase());
            smt.setString (3,alert.getDate().toTimeDataBase());
            smt.setInt (4,alert.getPriority());
            
           
            smt.executeUpdate();
        }
            conexao.close();
    }
    
    public void alterar (Alert alert) throws SQLException{
            String sqlalterar = "update alert set Texto=?, Data=?, Horario=?, Importancia=? where ID=?";
        try (PreparedStatement smt = conexao.prepareStatement(sqlalterar)) {
            smt.setString (1,alert.getText());
            smt.setString (2,alert.getDate().toDateDataBase());
            smt.setString (3,alert.getDate().toTimeDataBase());
            smt.setInt (4,alert.getPriority());
            
            smt.setInt (5,alert.getID());
           
            smt.executeUpdate();
        }
            conexao.close();
    }
    
    public void excluir(Alert alert) throws SQLException{
        String sqlexcluir = "DELETE FROM alert WHERE ID=?";
        PreparedStatement smt = conexao.prepareStatement(sqlexcluir);
        smt.setInt(1,alert.getID());
        smt.execute();
        smt.close();
        conexao.close();
    }
    
    //implementar getList
}
