/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution.dao;

import conection.Conection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import oficinas.processolution.DateProcess;
import oficinas.processolution.Product;

/**
 *
 * @author geova
 */
public class ProductDAO{
    private Connection conexao;

    public ProductDAO() throws ClassNotFoundException, SQLException{
        this.conexao = Conection.getConection();
    }

    public void incluir (Product product, boolean padrao) throws SQLException{
            String sqlincluir = "insert into product (Cor, Padrao, Data, Horario) values(?,?,?,?)";
        try (PreparedStatement smt = conexao.prepareStatement(sqlincluir)) {
            smt.setString (1,product.getSig());
            smt.setBoolean(2,padrao);
            smt.setString (3,product.getDate().toDateDataBase());
            smt.setString (4,product.getDate().toTimeDataBase());
            
            smt.executeUpdate();
        }
            conexao.close();
    }
    
    public void alterar (Product product,boolean padrao) throws SQLException{
            String sqlalterar = "update product set Cor=?, Padrao=?, Data=?, Horario=? where ID=?";
        try (PreparedStatement smt = conexao.prepareStatement(sqlalterar)) {
            smt.setString (1,product.getSig());
            smt.setBoolean(2,padrao);
            smt.setString (3,product.getDate().toDateDataBase());
            smt.setString (4,product.getDate().toTimeDataBase());
            
            smt.setInt (5,product.getID());
           
            smt.executeUpdate();
        }
            conexao.close();
    }
    
    public void excluir(Product product) throws SQLException{
        String sqlexcluir = "DELETE FROM product WHERE ID=?";
        PreparedStatement smt = conexao.prepareStatement(sqlexcluir);
        smt.setInt(1,product.getID());
        smt.execute();
        smt.close();
        conexao.close();
    }
    
    //implementar getList
}
