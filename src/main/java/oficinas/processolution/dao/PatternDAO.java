/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution.dao;

import conection.Conection;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.table.DefaultTableModel;
import oficinas.processolution.Pattern;

/**
 *
 * @author geova
 */
public class PatternDAO extends Pattern {

    private Connection conexao;

    public PatternDAO(String cor, Integer size, Integer qntd) throws ClassNotFoundException, SQLException {
        super(cor, size, qntd);
        this.conexao = Conection.getConection();
    }
    
    public PatternDAO() throws ClassNotFoundException, SQLException{
        this.conexao = Conection.getConection();
    }

    public Pattern searchPattern(Integer id) throws SQLException {
        String sql = "SELECT FROM pattern WHERE ID = ?";
        String cor="";
        Integer size=0, qntd=0;
        PreparedStatement ptm = this.conexao.prepareStatement(sql);
        ptm.setInt(1,id);
        ResultSet rs = ptm.executeQuery();
        while (rs.next()) {
            cor = rs.getString("Cor");
            size = rs.getInt("Size");
            qntd = rs.getInt("Qntd");
        }
        ptm.close();
        rs.close();
        conexao.close();
        return new Pattern(cor,size,qntd);
    }
    
    public Pattern createPattern(Integer id) throws SQLException{
        Pattern p = new Pattern();
        return searchPattern(id);
    }
}
