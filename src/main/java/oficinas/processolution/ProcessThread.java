/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import oficinas.processolution.dao.ProductDAO;

/**
 *
 * @author davi
 */
public class ProcessThread extends Thread {
    
    static final int X1 = 116;
    static final int X2 = 272;
    static final int X_CENTRAL = (X1 + X2)/2;
    static final int Y1_ESTEIRA1 = 12;
    static final int Y2_ESTEIRA1 = 66;
    static final int Y1_ESTEIRA2 = 106;
    static final int Y2_ESTEIRA2 = 159;
    static final int MENOR_AREA = 0;
    static final int MAIOR_AREA = 1000;
    
    private Frame[] frames;
    private int[] menoresX;
    private ArrayList<Product> novosProdutos;
    private ArrayList<Boolean> padrao;

    public ProcessThread(Frame[] frames, int[] menoresX) {
        this.frames = frames;
        this.menoresX = menoresX;
        novosProdutos = new ArrayList();
        padrao = new ArrayList();
    }
    
    public void novoObjeto(Product p, boolean estaDentroDoPadrao){
        try {
            novosProdutos.add(p);
            padrao.add(estaDentroDoPadrao);
            System.out.println("Novo Produto: " + p);
            ProductDAO p_dao = new ProductDAO();
            p_dao.incluir(p,estaDentroDoPadrao);
            //adicionar ao banco de dados que um novo objeto entrou - sig, estaDentrodoPadrao, data
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProcessThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ProcessThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @Override
    public void run() {
        Product p;
        Frame f;
        boolean esteira1jatemmenor = false, esteira2jatemmenor = false, estaDentroDoPadrao;
        int i, j, area;
        int menorXesteira1 = menoresX[0], menorXesteira2 = menoresX[1];
        for (i=0; i < frames.length; i++) {
            f = frames[i];
            for (j=0; j < f.getProducts().size(); j++) {
                p = f.getProducts().get(j);
                //ANALISA SE PRODUTO ESTÁ LOCALIZADO DEPOIS DO CENTRO
                if(p.getX() >= X_CENTRAL && p.getX() <= X2) {
                    //ESTEIRA 1
                    if(p.getY() >= Y1_ESTEIRA1 && p.getY() <= Y2_ESTEIRA1 && !esteira1jatemmenor){
                        if(p.getX() < menorXesteira1){
                            area = p.getWidth() * p.getHeight();
                            estaDentroDoPadrao = area >= MENOR_AREA && area <= MAIOR_AREA;
                            novoObjeto(p, estaDentroDoPadrao);
                        }
                        menorXesteira1 = p.getX();
                        esteira1jatemmenor = true;
                    }
                    //ESTEIRA 2
                    else if(p.getY() >= Y1_ESTEIRA2 && p.getY() <= Y2_ESTEIRA2 && !esteira2jatemmenor){
                        if(p.getX() < menorXesteira2){
                            area = p.getWidth() * p.getHeight();
                            estaDentroDoPadrao = area >= MENOR_AREA && area <= MAIOR_AREA;
                            novoObjeto(p, estaDentroDoPadrao);
                        }
                        menorXesteira2 = p.getX();
                        esteira2jatemmenor = true;
                    }
                }
            }
            esteira1jatemmenor = false;
            esteira2jatemmenor = false;
        }
        menoresX[0] = menorXesteira1;
        menoresX[1] = menorXesteira2;
        
            /*
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            Date hora = Calendar.getInstance().getTime(); // Ou qualquer outra forma que tem
            System.out.println(sdf.format(hora));
            for (int i=0;i<frames.length;i++)
            System.out.println(frames[i]);
            */
            /*GEO A LISTA TA PRONTA "novosprodutos" é uma lista com os novos produtos que entraram
         pega ela e faz o q quiser beijus*/
        /*
        try {   
            Alert.generateAlert(this.novosProdutos, this.padrao);
        } catch (SQLException ex) {
            Logger.getLogger(ProcessThread.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ProcessThread.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
    }
    
}
