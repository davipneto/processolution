/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution;

/**
 *
 * @author davi
 * @author geova
 */
public class DateProcess {
    
    private int day, month, year, hour, min, sec;

    public DateProcess(int year, int month, int day, int hour, int min, int sec) {
        this.day = day;
        this.month = month;
        this.year = year;
        this.hour = hour;
        this.min = min;
        this.sec = sec;
    }

    @Override
    public String toString() {
        return day + "/" + month + "/" + year + " " + hour + ":" + min + ":" + sec;
    }
    
    public String toDataBase() {
        String d = Integer.toString(day),
               mo = Integer.toString(month),
               h = Integer.toString(hour),
               mi = Integer.toString(min),
               s = Integer.toString(sec);
        if (day < 10)
            d = "0" + day;
        if (month < 10)
            mo = "0" + month;
        if (hour < 10)
            h = "0" + hour;
        if (min < 10)
            mi = "0" + min;
        if (sec < 10)
            s = "0" + sec;
        return (year + "-" + mo + "-" + d + h + ":" + mi + ":" + s);
    }
    
    public String toDateDataBase() {
        String d = Integer.toString(day),
               mo = Integer.toString(month);
        if (day < 10)
            d = "0" + day;
        if (month < 10)
            mo = "0" + month;
        return (year + "-" + mo + "-" + d);
    }
    
    public String toTimeDataBase() {
        String h = Integer.toString(hour),
               mi = Integer.toString(min),
               s = Integer.toString(sec);
        if (hour < 10)
            h = "0" + hour;
        if (min < 10)
            mi = "0" + min;
        if (sec < 10)
            s = "0" + sec;
        return (h + ":" + mi + ":" + s);
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }
    
}
