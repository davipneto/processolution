/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oficinas.processolution;

import java.sql.SQLException;
import java.util.ArrayList;
import oficinas.processolution.dao.AlertDAO;
import oficinas.processolution.dao.PatternDAO;

/**
 *
 * @author geova
 */
public class Alert {

    private String text;
    private DateProcess date;
    private Integer priority;
    private String mesage;
    private Integer ID;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public DateProcess getDate() {
        return date;
    }

    public void setDate(DateProcess date) {
        this.date = date;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getMesage() {
        return mesage;
    }

    public void setMesage(String mesage) {
        this.mesage = mesage;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public Alert(String text, DateProcess date, Integer priority, String mesage) {
        this.text = text;
        this.date = date;
        this.priority = priority;
        this.mesage = mesage;
    }

    public static void generateAlert(ArrayList<Product> novosProdutos, ArrayList<Boolean> padrao) throws SQLException, ClassNotFoundException {
        Pattern pattern = new Pattern();
        PatternDAO p = new PatternDAO();
        pattern = p.createPattern(1);
        AlertDAO alert = new AlertDAO();
        Integer interv = (pattern.getQntd() / 360)*60;//em segundos
        int time1_temp,time2_temp;
        //testar cor, testar tamanho, testar tempo
        //Prioridades: 0-quantidade,1-tamanho,2-cor
        for (int i = 0; i < novosProdutos.size(); i++) {
            //compara cor com o padrão
            if (novosProdutos.get(i).getSig().compareTo(pattern.getCor()) != 0) {
                Alert a = new Alert("Alerta " + i, novosProdutos.get(i).getDate(), 2, "Fora do padrão de cor");
                alert.incluir(a);
            }//pega a variavel de esta fora do padrao ou nao para comparação do tamanho
            if (!padrao.get(i)) {
                Alert a = new Alert("Alerta " + i, novosProdutos.get(i).getDate(), 1, "Fora do padrão de tamanho");
                alert.incluir(a);
            }
            //compara o intervalo entre o anterior e o proximo com o padrao e veja se é muito maior
            if (i > 0) {
                time1_temp = novosProdutos.get(i).getDate().getMin()*60+novosProdutos.get(i).getDate().getSec();
                time2_temp = novosProdutos.get(i - 1).getDate().getMin()*60 + novosProdutos.get(i - 1).getDate().getSec();
                                
                if ((time1_temp - time2_temp) < interv-30 || (time1_temp - time2_temp) > interv+30) {
                    Alert a = new Alert("Alerta " + i, novosProdutos.get(i).getDate(), 0, "Fora do padrão de quantidade");
                    alert.incluir(a);
                }
            }else{
                time1_temp = novosProdutos.get(i).getDate().getMin()*60+novosProdutos.get(i).getDate().getSec();
                
                if (time1_temp < interv-30 || time1_temp > interv+30) {
                    Alert a = new Alert("Alerta " + i, novosProdutos.get(i).getDate(), 0, "Fora do padrão de quantidade");
                    alert.incluir(a);
                }
            }
            //seria legal acrescentar o numero do alerta de acordo com o numero do ultimo alerta no banco
            

        }

    }
}
